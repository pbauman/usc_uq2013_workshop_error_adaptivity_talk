\documentclass[mathserif]{beamer}
\usetheme[secheader]{pecostalk}
\graphicspath{{figs/}}

\usepackage{verbatim}
\usepackage{color}
\usepackage{subfigure}

% Need this for the Stokes-NS example
\input{./beamerheader}
\pgfdeclarehorizontalshading{rainbow}{100bp}
    {color(0bp)=(blue); color(25bp)=(blue); color(38bp)=(cyan); color(50bp)=(green);
    color(62bp)=(yellow); color(75bp)=(red); color(100bp)=(red)}
  \newcommand{\colorbar}[3]{
    \begin{tikzpicture}[shading=rainbow,scale=0.5,font=\footnotesize]
    \shade (0,0) rectangle node[below] {#1} (10,.4);
    \draw (10,.4) -- (10,-.5) node[below] {#2};
    \draw (0,.4) -- (0,-.5) node[below] {#3};
    \end{tikzpicture}
    }
    
  \newcommand{\figheight}{0.43}
  \newcommand{\ins}{^\mathrm{n}}
  \newcommand{\sto}{^\mathrm{s}}
  \renewcommand{\Re}{\mathrm{Re}}
  \newcommand{\fapprox}{_0}%_\Sigma}
  \newcommand{\uapprox}{\bs{u}\fapprox}
  \newcommand{\papprox}{p\fapprox}
  \newcommand{\dapprox}{}%_S}
  \newcommand{\zapprox}{\bs{z}\dapprox}
  \newcommand{\zetaapprox}{\zeta\dapprox}
  \newcommand{\Space}{V \! \times \! Q}
  \newcommand{\QoI}{\mathcal{Q}}
  \newcommand{\Residual}{\mathcal{R}}
  \newcommand{\vstretch}{\phantom{\biggr[\biggl.}}
  \newcommand{\Slf}{\mathcal{N}}
  \newcommand{\semicol}{\mathrm{;}}
  \newcommand{\col}{\mathrm{:}}
  \newcommand{\plotelems}{
    % verticals
    \draw[black!40] (0.0,-.3)--(0.0,2.);
    \draw[black!40] (1.0,-.3)--(1.0,2.);
    \draw[black!40] (2.0,-.3)--(2.0,2.);
    \draw[black!40] (3.0,-.3)--(3.0,2.);
    \draw[black!40] (3.5,-.3)--(3.5,2.);
    \draw[black!40] (4.0,-.3)--(4.0,2.);
    \draw[black!40] (4.5,-.3)--(4.5,2.);
    % x-axis
    \draw (-.3,0.)--(4.8,0.);
    % elem labels
    \node[below] at (1.50,0.0) {$\kappa_0$};
    \node[below] at (3.75,0.0) {$\kappa_1$}; 
    }
    
\definecolor{tuered}{rgb}{0.839,0.000,0.290}
\definecolor{tueblue}{rgb}{0.000,0.400,0.800}

\newcommand{\red}[1]{{\color{red}#1}}

\newcommand{\UQ}[1]{{\color{red}{#1}}}

\newcommand{\pdv}[2]{\frac{\partial #1}{\partial #2}}
%\newcommand{\grad}[1]{\bv{\nabla} {#1}}
\newcommand{\bv}[1]{{\ensuremath{\boldsymbol{#1}}}}
\newcommand{\bt}[1]{{\ensuremath{\boldsymbol{#1}}}}

\newcommand{\sa}{\nu_{\mathrm{sa}}}
\newcommand{\pp}[2]{\frac{\partial #1}{\partial #2}}

\newcommand{\norm}[1]{\| #1 \|}

\newcommand{\software}[1]{\texttt{#1}}
\newcommand{\code}[1]{\software{#1}}
\newcommand{\GRINS}{\software{GRINS}}
\newcommand{\libMesh}{\software{libMesh}}

\newcommand{\todo}[1]{{\color{red} TODO: #1}}
\newcommand{\bgamma}{\bar{\gamma}}

\newcommand{\M}[1]{{\mathbf #1}}
\newcommand{\V}[1]{{\mathbf #1}}

\usepackage[version=3]{mhchem}
\usepackage{tikz}%  these two lines
\mhchemoptions{arrows=pgf-filled}% because I prefer pgf arrows

\setbeamertemplate{bibliography item}[text]

\date{August 14, 2013}
\author[P. T. Bauman]{Paul T. Bauman}
\institute{Institute for Computational Engineering and Sciences\\The University of Texas at Austin
\\[0.05in]
Mechanical and Aerospace Engineering\\
%Computational and Data-Enabled Science and Engineering Program\\
State University of New York at Buffalo}
\title[Error Estimation and Adaptivity]{A Brief Survey of Error Estimation and Adaptivity}

%===============================================================================
% Begin Document
%===============================================================================
\begin{document}

%===============================================================================
% Title Slide
%===============================================================================
\begin{frame}
\begin{center}
\includegraphics[width=.8\linewidth]{grand_logo}\\
\end{center}
\vspace{-0.2in}
\titlepage
\vspace{-0.6in}
\includegraphics[width=0.15\linewidth]{ICES-secondary-logo-crop}
\hspace*{\fill}
\includegraphics[scale=0.2]{asc_logo}
\end{frame}

%===============================================================================
% Objectives
%===============================================================================
\section{Objectives}
\begin{frame}
\frametitle{Course Objectives}

\begin{block}{Scope}
\begin{itemize}
\item Broad overview of
	\begin{itemize}
	\item error estimation techniques for estimating error in mathematical
          models
	\item adaptation strategies for controlling error
	\end{itemize}
\item Brief survey of some contemporary research areas
\end{itemize}
\end{block}

\begin{block}{Target Audience}
\begin{itemize}
\item PhD students/Postdocs with some exposure to numerical methods
\item Limited functional analysis training
\end{itemize}
\end{block}

\begin{block}{Slides}
\begin{itemize}
\item Slides are publicly available at: \url{https://bitbucket.org/pbauman/usc_uq2013_workshop_error_adaptivity_talk}
\end{itemize}
\end{block}

\end{frame}


%===============================================================================
% Course Notes
%===============================================================================
\begin{frame}
\frametitle{Course Notes}

\begin{block}{This is for you!}
\begin{itemize}
\item Please stop me to ask questions
\item Please stop me to have a discussion
\item Please download the slides at your convenience
\end{itemize}
\end{block}

\begin{block}{Check the References}
\begin{itemize}
\item I have chosen to pursue a breadth of topics instead of more depth in fewer topics
\item I've tried to provide a list of references for you to pursue your own study
\end{itemize}
\end{block}

\end{frame}

%===============================================================================
% Primary References
%===============================================================================
\begin{frame}
\frametitle{Primary References}

\begin{block}{}
\begin{itemize}
\item Oden/Ainsworth Book on \emph{a posteriori} error estimation~\cite{OdenAinsworth}
\item Bangerth/Rannacher lecture notes on adaptive finite elements~\cite{BangerthRannacher2003}
\item Carey Computational Grids book~\cite{Carey1997}
\item Becker/Rannacher paper on optimal control approach to \emph{a posteriori} error estimation~\cite{BeckerRannacher2001}
\item Oden/Prudhomme paper on modeling error~\cite{OdenPrudhomme2002}
\item Excellent short courses by Don Estep available on his webpage: \url{http://www.stat.colostate.edu/~estep/}
\end{itemize}
\end{block}

\end{frame}

\section{Outline}
%===============================================================================
% Outline
%===============================================================================
\begin{frame}
\frametitle{Outline}

\begin{block}{}
\begin{itemize}
\item Overview
\item Preliminaries
\item Error control through mesh adaptation
\item Error estimation for discretized PDE's
	\begin{itemize}
	\item Global error estimators
	\item Adjoint-based error estimators
	\end{itemize}
\item Modeling error estimation and model adaptivity
	\begin{itemize}
	\item Coupled Stokes/Navier-Stokes example
	\item Coupled particle/continuum multiscale model example
	\end{itemize}
\item Some available software
\item Contemporary issues
	\begin{itemize}
	\item QoI challenges
	\item Unsteady problems
	\item Extending to UQ setting
	\end{itemize}
\end{itemize}
\end{block}

\end{frame}

%===============================================================================
% Overview
%===============================================================================
\section{Overview}
\input{overview}

%===============================================================================
% Preliminaries
%===============================================================================
\section{Preliminaries}
\input{preliminaries}

%===============================================================================
% Error Control through Mesh Adaptation
%===============================================================================
\section{Error Control through Mesh Adaptation}
\input{mesh_adapt}

%===============================================================================
% Error Estimation for Discretized PDE's
%===============================================================================
\section{Error Estimation for Discretized PDE's}
\input{discrete_error}
\input{adjoint_discrete_error}

%===============================================================================
% Modeling Error Estimation
%===============================================================================
\section{Modeling Error Estimation and Adaptivity}
\input{model_error}
\input{model_adapt}

\subsection{Example: Coupled Stokes/Navier-Stokes}
\input{stokes_ns}

\subsection{Example: Coupled Particle/Continuum Multiscale Models}
\input{arlequin}

%===============================================================================
% Software
%===============================================================================
\section{Software}
\input{software}

%===============================================================================
% Current Trends
%===============================================================================
\section{Contemporary Issues}
\input{qoi_current}
\input{unsteady_adjoint}
\input{uq_apps}

%===============================================================================
% Fin
%===============================================================================
\begin{frame}
\frametitle{}
\begin{block}{}
\center{Thank you!} \\
\center{\href{mailto:pbauman@buffalo.edu}{\nolinkurl{pbauman@buffalo.edu}}}
\end{block}
\end{frame}

%===============================================================================
% References
%===============================================================================
\section{References}
\begin{frame}[allowframebreaks]
\frametitle{References}

\bibliographystyle{plain}
\bibliography{refs}

\end{frame}


\end{document}
%===============================================================================
% END OF FILE
%=============================================================================== 

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{QoI-based Error Estimation}

\begin{block}{What do you really want to compute?}
\begin{itemize}
\item Thus far, have focused on error estimates for $\norm{u - u^h}$
\item Very often, we're not interested in $u$ itself, but rather some
      \emph{functional} of $u$, $Q(u)$
\item We call $Q(u)$ the \emph{quantity of interest}
\item Examples:
  \begin{itemize}
    \item Stress in a local region
    \item Maximum heat flux on a surface
    \item Local vorticity
  \end{itemize}
\item Thus, we would instead prefer to estimate the error $Q(u) - Q(u^h)$
\item One theory for developing such error estimates follows an ``optimal control approach''
\end{itemize}
\end{block}

\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{QoI-based Error Estimation}

\begin{block}{Abstract Nonlinear Problem}
Find $u \in V$ such that
\begin{equation*}
B(u;v) = F(v), \quad \forall v \in V
\end{equation*}
\end{block}

\begin{block}{Optimal Control Approach}
Find $u \in V$ such that
\begin{equation*}
Q(u) = \inf_{v \in M} Q(v)
\end{equation*}
where
\begin{equation*}
M =  \left\{ v \in V; \; B(v;q) = F(q) \;\; \forall q \in V \right\}
\end{equation*}
\end{block}

\[
\begin{array}{lcl}
Q'(u;v) & = & \displaystyle \lim_{\theta \rightarrow 0}
                \theta^{-1}
                [Q(u+\theta v) - Q(u)]
\\[0.1in]
B'(u;v,w) & = & \displaystyle \lim_{\theta \rightarrow 0}
                \theta^{-1}
                [B(u+\theta v; w) - B(u;w)]
\end{array}
\]


\end{frame}


%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{QoI-based Error Estimation}

\begin{block}{Comments on Optimal Control Approach}
\begin{itemize}
\item ``Trivial'' optimal control problem: Compute $u$ from $B(u;v) = F(v)$ and
      evaluate $Q(u)$
\item What this buys us is the machinery of optimal control for deriving equations
      needed for error computation.
\end{itemize}
\end{block}

\begin{block}{Lagrangian}
Must find saddle point $(u,p) \in V \times V$ of Lagrangian
\begin{equation*}
L(u,p) = Q(u) + F(p) - B(u;p)
\end{equation*}
\end{block}

\begin{block}{Critical Points}
\begin{equation*}
L^{\prime}( (u,p);(v,q) ) = 0, \qquad \forall (v,q) \in V \times V
\end{equation*}
\end{block}

\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{QoI-based Error Estimation}

\begin{block}{Critical Points}
\vspace{-1em}
\begin{equation*}
L^{\prime}( (u,p);(v,q) ) = Q^{\prime}(u; v) - B^{\prime}(u; v, p) + F(q) - B(u;q), \; \forall (v,q) \in V \times V
\end{equation*}
\end{block}

\begin{block}{Primal Problem (potentially nonlinear)}
Find $u \in V$ such that
\begin{equation*}
B(u;q) = F(q), \quad \forall q \in V
\end{equation*}
\end{block}

\begin{block}{Adjoint or Dual Problem (\emph{always linear})}
Find $p \in V$ such that
\begin{equation*}
B^{\prime}(u;v,p) = Q^{\prime}(u;v), \quad \forall v \in V
\end{equation*}
\end{block}

\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{QoI-based Error Estimation}

\begin{block}{Residual}
\begin{equation*}
R(u;v) = F(v) - B(u;v), v \in V
\end{equation*}
\end{block}

\begin{block}{Error Representation}
\begin{equation*}
Q(u) - Q(u^h) = R(u^h; p) + \Delta
\end{equation*}
where $u^h \in V^h \subset V$ is our discrete approximation to $u$, $p$ is the adjoint
solution, and $\Delta$ is higher order in $\norm{u - u^h}$
\end{block}

\begin{block}{Comments}
\begin{itemize}
\item This error representation requires the \emph{exact} adjoint solution $p$
\item $p$ also requires a discrete approximation
\item But, the second argument of the residual operator is linear, so...
\end{itemize}
\end{block}

\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{QoI-based Error Estimation}

\begin{block}{Computable Error Representation}
\vspace{-0.15in}
\begin{equation*}
\begin{split}
Q(u) - Q(u^h) &= R(u^h; p^h) + R(u^h; p - p^h) + \Delta \\
Q(u) - Q(u^h) &\approx R(u^h; p^h)
\end{split}
\end{equation*}
where $p^h$ is our discrete approximation to $p$
\end{block}

\begin{block}{Comments}
\begin{itemize}
\item When we neglect $\Delta$, we are assuming that $u^h$ is sufficiently close to $u$ that it is small
  \begin{itemize}
  \item $u^h$ must be a ``good enough'' approximation of $u$ to get good error estimators
  \end{itemize}
\item What happens if we use the same mesh for the solution of $p^h$ as $u^h$?
  \begin{itemize}
  \item $\Rightarrow$ $p^h \in V^h$
  \item But $u^h \in V^h$ such that $R(u^h; v^h) = 0, \; \forall v^h \in V^h$
  \item $\Rightarrow$ $R(u^h; p^h) = 0$! No information in the error estimate!
  \end{itemize}
\end{itemize}
\end{block}

\end{frame}


%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{QoI-based Error Estimation}

\begin{block}{What do we do, then?}
\begin{itemize}
\item Need to ``enrich'' the adjoint solution beyond the space $V^h$
\pause
\item One strategy is to do one or more uniform refinements of the mesh and compute
      the adjoint solution, $p^{h/2}$, on that mesh
      \begin{itemize}
      \item This is, probably, the most common strategy
        \pause
      \item Then, $Q(u) - Q(u^h) \approx R(I^{h/2}u^h; p^{h/2})$, where $I^{h/2}u^h$ is the interpolant of
            $u^h$ onto the uniformly refined grid
      \end{itemize}
\item Another common strategy involves using higher order basis functions, i.e. raising the order ``p'' of the
      approximation
\item Other strategies abound such a using residual bubbles
\end{itemize}
\end{block}

\end{frame}


%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Summary}

\begin{block}{Error Estimation Algorithm}
\begin{itemize}
\item[1.] Compute the primal solution $u^h$: $B(u^h; v^h) = F(v^h), \forall v^h \in V^h$
\pause
\item[2.] Enrich the solution space, e.g. $h \rightarrow h/2$
\pause
\item[3.] Compute the adjoint solution $p^{h/2}$: $B^{\prime}( I^{h/2}u^h; v^{h/2}, p^{h/2}) = Q^{\prime}( I^{h/2}u^h; v^{h/2} ), \forall v^{h/2} \in V^{h/2}$
\pause
\item[4.] Evaluate the residual to computer error estimate:  $Q(u) - Q(u^h) \approx R(I^{h/2}u^h; p^{h/2})$
\end{itemize}
\end{block}

\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Example}

\begin{block}{Convection-Diffusion Problem with Adjoints}
Equation:
\begin{equation*}
\nabla \cdot \alpha \nabla \V{u} + \beta \vec{e}_x \cdot \nabla
\V{u} + \V{f} = 0
\end{equation*}
Manufactured solution$^*$:
\vspace{-0.1in}
\begin{equation*}
\V{u} \equiv 4(1 - e^{-\alpha x} - (1 - e^{-\alpha})x)y(1-y)
\end{equation*}
\vspace{-0.3in}
\begin{itemize}
\item $\alpha$ controls flux strength, layer
\end{itemize}
\end{block}

\begin{block}{$^*$Aside: MASA}
\begin{footnotesize}
\begin{itemize}
\item Manufactured and Analytical Solution Abstraction - library to facilitate
      the use of the Method of Manufactured Solutions (MMS)
\item N. Malaya, K. C. Estacio-Hiroms, R. H. Stogner, K. W. Schulz, P. T. Bauman, G. F. Carey, 
      ``MASA: A Library for Verification Using Manufactured and Analytical Solutions",
      Engineering with Computers, in press, \url{http://dx.doi.org/10.1007/s00366-012-0267-9.}
\item Available: \url{https://red.ices.utexas.edu/projects/software/wiki/MASA}
\end{itemize}
\end{footnotesize}
\end{block}

\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Example}

\begin{columns}
\begin{column}{0.5\textwidth}
\begin{block}{Convergence}
\centerline{\includegraphics[width=\linewidth]{qoi_may_29_2009_convergence_cropped}}
\end{block}
\end{column}
%
\begin{column}{0.4\textwidth}
\only<1>{
\begin{block}{Primal Solution}
\centerline{\includegraphics[width=\linewidth]{QoI_October_20_2011_kelly_mesh}}
\end{block}
}
\only<2>{
\begin{block}{Adjoint Solution}
\centerline{\includegraphics[width=\linewidth]{QoI_May_29_2009_arpp_mesh+adjoint}}
\end{block}
}
\end{column}
\end{columns}

\vfill

\begin{footnotesize}
$^*$Images courtesy Roy H. Stogner, The University of Texas at Austin
\end{footnotesize}

\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Example}

\begin{block}{Comments}
\begin{itemize}
\item Flux jump error indicator picks up the sharp layer near the boundary
\item Convergence of QoI \emph{stalls}
\item Adjoint-based error estimator still picks up layer, but also picks up influence
      of QoI, as expected
\item Convergence of adjoint-based error estimator double the rate of the uniformly
      refined solution
\end{itemize}
\end{block}

\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Adjoint Problem}

\begin{block}{Definition of Adjoint Operator}
\begin{itemize}
\item Definition of adjoint operator: $(Lu,v) = (u,L^*v)$
\item Example: linear system $Ax = b$, adjoint of $A$ is $A^T$
\item Example: differential equation $Lu = -u^{\prime\prime} + u^{\prime}$, $L^*v = -v^{\prime\prime} - v^{\prime}$
\end{itemize}
\end{block}

\pause

\begin{block}{Why is the Adjoint Problem Called the Adjoint Problem?}
We use a linear system as a simple example
\begin{itemize}
\item Exact: $Ax = b$
\item Approximate: $Ax_0 = b$
\item Error: $e = x - x_0$
\item Equation for error: $Ae = r$, $r = b - Ax_0$
\item Adjoint problem: $A^T p = q$, for some $q$ (QoI)
\item \emph{a posteriori} error estimate: $|(e,q)| = |(e,A^T p)| = |(Ae,p)| = |(R,p)|$
\end{itemize}
\end{block}

\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Adjoint Solution}

The adjoint solution may look somewhat familiar

\pause

\begin{block}{Green's Functions}
If we are computing
\begin{equation*}
Lu = f \; x \in \Omega
\end{equation*}
plus boundary and/or initial conditions, then the \emph{Green's function} $G$ is
defined as
\begin{equation*}
L^* G(y,x) = \delta_y(x),  \; x \in \Omega
\end{equation*}
plus boundary and/or initial conditions
\end{block}

\pause

\begin{block}{Comments}
\begin{itemize}
\item If our quantity of interest is a point value, then the adjoint solution is actually a Green's function of our operator!
	\begin{itemize}
	\item Point value QoI's were studied for elliptic problems in~\cite{PrudhommeOden1999}
	\end{itemize}
\end{itemize}
\end{block}

\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Adjoint Solution}

\begin{block}{More Comments}
\begin{itemize}
\item For other QoI's, the adjoint solution is, essentially, a generalized Green's function
\item Often referred to as generalized Green's function in the literature
\item Excellent discussion in Estep short course here: \url{http://www.stat.colostate.edu/~estep/research/talks/adjointcourse.pdf}
\end{itemize}
\end{block}

\begin{block}{So why the ``optimal control approach"?}
\begin{itemize}
\item There is no unique definition for the adjoint of a nonlinear operator
\item The choice made is the most common one, but others are possible
\item Extends the linear case
\item Gives a systematic framework for handling nonlinear problems
	\begin{itemize}
	\item You're probably not solving linear equations!
	\end{itemize}
\end{itemize}
\end{block}

\end{frame}


%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Alternative Adjoint-Based Error Indicators}

\begin{block}{Bound on Error}
See~\cite{BangerthRannacher2003}, for example
\vspace{-0.15in}
\begin{equation*}
|R(u^h; v^h, p)| \le \sum_K \norm{R_K} \norm{u - u^h} \norm{p - p^h}
\end{equation*}
\end{block}

\begin{block}{Comments}
\begin{itemize}
\item Although the actual error estimate is much more inaccurate than the form
      we've been discussing, this can be very convenient as an error indicator
\item In particular, we can solve the adjoint problem on the same grid!
\item Use global error estimators to estimate $\norm{u - u^h}$ and $\norm{p - p^h}$
\item Gives an error indicator for QoI-based adaptivity
\item Good adaptive strategy might be to use such an indicator for a few iterations to drive
      adaptivity, then switch to the more accurate error estimator
\end{itemize}
\end{block}

\end{frame}


%===============================================================================
% END OF FILE
%===============================================================================

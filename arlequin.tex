%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Example: Coupled Particle/Continuum Multiscale Model}

\begin{block}{Motivation}
\begin{itemize}
\item Study multiscale models of polymer deformation that is a component of
      a lithography process in semiconductor manufacturing
      \begin{itemize}
      \item These models involve coupling of particle models with continuum, nonlinear
            elasticity models
      \end{itemize}
\item Study modeling error estimation and adaptivity in this context
\item This application illustrates some of the additional complexities one faces
      when studying modeling error estimation
\end{itemize}
\end{block}

\begin{block}{References}
\cite{Bauman2008, BaumanOdenetal2009, PrudhommeBaumanetal2008, OdenPrudhommeetal2005, PrudhommeChamoinetal2009}
\end{block}

\begin{footnotesize}
\begin{block}{Acknowledgements}
\begin{itemize}
\item J. Tinsley Oden - The University of Texas at Austin
\item Serge Prudhomme - Ecole Polytechnique de Montr\'eal
\end{itemize}
\end{block}
\end{footnotesize}


\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Particle Configuration}

\begin{center}
\includegraphics[width=0.7\textwidth]{21x101x21_combo}
\end{center}

\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Base Model}

{\bfseries Given:}

\begin{enumerate}
\item
Lattice with $N$ molecules, each with position ${x}_i$.
\item
$N_i$ neighbors/molecule.
\item
$E_{ik}({x}_i,{x}_k) = $ potential energy between particle $i$ and neighbor~$k$.
\end{enumerate}


\vspace{.1in}
{\bfseries Goal:}
\[
%$
\text{Find}\ {x}^{*} = \text{arg}\min\limits_{{x}} E({x})
%$
\]
where
\[
E({x}) = \sum\limits_{i=1}^N \sum\limits_{k=1}^{N_i} 
E_{ik}({x}_i,{x}_k)
\]

{\bfseries System of equations:}
\[
%\fcolorbox{Black}{Apricot}{
%$
\displaystyle
\frac{\partial E}{ \partial x} (x^*)= f(x^*) = 0
%$%}
\]


\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Base Model Written in Weak Form}

{\bfseries Base Primal and Adjoint Problems:} For $u$, $v$, $p \in \mathbb R^{3N}$,
\begin{minipage}{0.65\textwidth}
\[
\begin{split}
& B(u;v) = \sum\limits_{i=1}^N 
                   \sum\limits_{k=1}^{N}   \frac{\partial E_{ik} }{ \partial {u_i}} \cdot v_i \\
& B'(u;v,p) = \sum\limits_{i=1}^N 
                \sum\limits_{j=1}^N 
                 v_j \cdot 
                \sum\limits_{k=1}^{N}  \frac{\partial^2 E_{ik}}{ \partial {u_i}\partial {u_j}}
                 \; p_i \\
& Q(u) = \frac{1}{M} \sum\limits_{m=1}^{M} u_m \cdot e_1                                                                                                                                                                                   
\end{split}
\]
\end{minipage}
\begin{minipage}{0.30\textwidth}
\includegraphics[width=\textwidth]{cube}
\end{minipage}


\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Arlequin Method}

\begin{center}
\includegraphics[width=0.62\textwidth]{arlequin}\\[0.1in]
\includegraphics[width=0.58\textwidth]{alpha}
\end{center}

\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Arlequin Method}

\vspace{-0.2in}
\[
\renewcommand{\arraystretch}{1.2}
\boxed{
\begin{split}
& \text{Find $(u,w) \in X$, $\lambda \in M$ such that:}\\
& 
\begin{array}{rcll}
a\left( (u,w), (v,z) \right) + b\left( \lambda, (v,z) \right) &=&  l((v,z)) & \; \forall (v,z) \in X \\
b\left( \mu, (u , w) \right) &= & 0 & \; \forall \mu \in M
\end{array}
\end{split}
}
\]

where
\vspace{-0.2in}
\small
\[                                                                                                                                                                                                                                         
\begin{split}
&a\left( (u,w), (v,z) \right) = \int_{\Omega_c} \red{\alpha_c} W^{\prime} (u) v^{\prime} dx 
  + \sum_{i=1}^m \red{\alpha_d} E_i^{\prime} ( w_i , w_{i-1} ) (z_i - z_{i-1}) \\ 
&b\left(\mu, (v,z) \right) = \int_{\Omega_o} \red{\beta_1} \mu  (v - \Pi z) + 
\red{\beta_2} \mu^{\prime}  (v - \Pi z)^{\prime} dx \\
&l((v,z)) = fz_{m}
\end{split}
\]


\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Example 3D Arlequin Calculation}

\begin{columns}
\begin{column}{0.45\textwidth}
\begin{block}{Solution}
\begin{center}
\includegraphics[width=\textwidth]{poly51}
\end{center}
\end{block}
\end{column}
%
\begin{column}{0.45\textwidth}
\begin{block}{Comments}
\begin{itemize}
\item Reduced 375,000 unknowns to 18,228
\item ~5 mins. on a workstation from 1.4 CPU hours on 32 processors
\item A factor of ~21 time and cost
\end{itemize}
\end{block}
\end{column}
\end{columns}

\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Error Estimation}

\begin{equation*}
Q(u) - Q(u_0) =  \only<1,2>{R}\only<3>{\red{R}}(\only<1,3>{u_0}\only<2>{\red{u_0}}; \only<1,2>{p}\only<3>{\red{p}}) + \Delta
\end{equation*}

\begin{columns}
\begin{column}{0.45\textwidth}
\begin{block}{\only<1,3>{Surrogate Solution}\only<2>{\red{Surrogate Solution}}}
\centerline{\includegraphics[width=\textwidth]{arlequin_small}}
\end{block}
\end{column}
%
\begin{column}{0.45\textwidth}
\only<3>{
\begin{block}{\red{Base Solution}}
\centerline{\includegraphics[width=\textwidth]{particle}}
\end{block}
}
\end{column}
\end{columns}

\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Error Estimation}

\begin{block}{Comments}
\begin{itemize}
\item The surrogate solution is \emph{not} compatible as is with the base model
      residual operator
      \begin{itemize}
      \item Surrogate solution is a mixed particle and continuum model
      \item Base model residual is only defined for particles
      \end{itemize}
\pause
\item Furthermore, we require the fine scale adjoint solution
      \begin{itemize}
      \item Adjoint problem requires surrogate solution (because we don't have the exact solution)
      \item Again, base model adjoint equations only defined for particles
      \end{itemize}
\end{itemize}
\end{block}

\pause

\vspace{0.1in}
\begin{center}
We must make the surrogate solution compatible with the base model operators! \\
\end{center}

\pause

\vspace{0.05in}

\begin{center}
We will use interpolation
\end{center}

\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Interpolant}


\begin{columns}
\begin{column}{0.25\textwidth}
\begin{block}{Model Partition}
\centerline{\includegraphics[width=\textwidth]{arlequin_partition}}
\end{block}
\end{column}
%
\begin{column}{0.6\textwidth}
\begin{block}{Comments}
\begin{itemize}
\item Must choose how to interpolate each of the three regions
	\begin{itemize}
	\item Particle region (pink)
	\item Overlap region (red)
	\item Continuum region (green)
	\end{itemize}
\end{itemize}
\end{block}
\end{column}
\end{columns}

\pause

\begin{columns}
\begin{column}{0.8\textwidth}
\begin{block}{Particle Interpolation}
\centerline{\includegraphics[width=\textwidth]{particle_interpolation}}
\end{block}
\end{column}
\end{columns}

\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Interpolant}

\begin{columns}
\begin{column}{0.8\textwidth}
\begin{block}{Continuum Mapping}
\centerline{\includegraphics[width=\textwidth]{continuum_map}}
\end{block}
\end{column}
\end{columns}

\begin{block}{Comments}
\begin{itemize}
\item Continuum model is large deformation
\item Displacement gives mapping from reference configuration to current configuration
      (in continuum mechanics parlance)
\item Use this mapping to construct interpolant in continuum region
\end{itemize}
\end{block}

\end{frame}


%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Interpolant}

\begin{columns}
\begin{column}{0.8\textwidth}
\begin{block}{Continuum Interpolant}
\centerline{\includegraphics[width=\textwidth]{arlequin_interpolation}}
\end{block}
\end{column}
\end{columns}


\end{frame}


%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Error Estimation}

\only<1,2,3>{
\begin{columns}
\begin{column}{0.8\textwidth}
\begin{block}{Surrogate Interpolant}
\begin{itemize}
\item We have now constructed an interpolant for the surrogate solution, $\Pi u_0$, that
      takes us back to the base model configuration
\pause
\item We can now pose the adjoint problem 
      $B^{\prime}( \Pi u_0; v, \hat{p} ) = Q^{\prime}( \Pi u_0, v)$
      \begin{itemize}
      \item We use the $\hat{\cdot}$ notation to delineate that we've used the interpolated
            surrogate solution to compute the adjoint
      \end{itemize}
\pause
\item And evaluate the residual $R(\Pi u_0; \hat{p})$ to estimate the error
\end{itemize}
\end{block}
\end{column}
\end{columns}
}

\only<4>{
\begin{columns}
\begin{column}{0.5\textwidth}
\begin{block}{Adjoint Solution $\hat{p}$}
\centerline{\includegraphics[width=\textwidth]{particle_dual}}
\end{block}
\end{column}
\end{columns}
}

\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Error Estimation}

\begin{block}{The Problem}
\begin{itemize}
\item In principle, the previous strategy is not feasible
\pause
	\begin{itemize}
	\item Solving the base model adjoint problem and evaluating the base model residual
	      is not tractable!
	\end{itemize}
\pause
\item We need a different strategy that is both tractable and effective for error
      estimation
\end{itemize}
\end{block}

\pause

\begin{block}{What About the Surrogate Model?}
\begin{itemize}
\item Adjoint problem: $B_0^{\prime}( u_0; v_0, p_0 ) = Q^{\prime}( u_0, v_0)$
\pause
\item Residual evaluation: $Q(u) - Q(u_0) \approx R_0(u_0;p_0)$
\pause
$= 0$!
\pause
	\begin{itemize}
	\item Analogous to Galerkin orthogonality in the discretized PDE case!
	\end{itemize}
\end{itemize}
\end{block}

\pause

\begin{center}
Conclusion:  We need to solve the dual problem and evaluate the residual on something 
``between" the coarse model and the full lattice.
\end{center}

\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Enrichment}

\begin{center}
\includegraphics[width=0.8\textwidth]{arlequin_enrichment}
\end{center}

\only<1,2,3>{
\begin{block}{Strategy}
\begin{itemize}
\item ``Enrich" the original coarse model to include more particle regions
\pause
\item Use the same interpolation procedure as before
\pause
\item Solve the adjoint problem and evaluate the residual on this configuration
\end{itemize}
\end{block}
}

\only<4>{
\begin{block}{``Enriched" Adjoint and Residual}
\begin{itemize}
\item Adjoint: $\tilde{B}^{\prime}( \tilde{\Pi}u_0; \tilde{v}, \tilde{p} ) = Q^{\prime}( \tilde{\Pi}u_0, \tilde{v})$
\pause
\item Residual: $Q(u) - Q(\tilde{\Pi}u_0) \approx \tilde{R}(\tilde{\Pi}u_0;\tilde{p})$
\end{itemize}
\end{block}
}


\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Example Calculations}

``Smooth Problem"
%\begin{block}{``Smooth Problem"}
\begin{table}[width=\textwidth]
\begin{tabular}{|c|c|c|c|c|}
\hline
$\eta_{eff}$ & $\eta_{eff}$ & $\eta_{eff}$ & $\eta_{eff}$ & \footnotesize{\# of Enrich.} \\
\hline
$\frac{|R(\Pi u_0, p)|}{|Q(u) - Q(\Pi u_0)|}$ &  
$\frac{|R(\Pi u_0, \hat{p})|}{|Q(u) - Q(\Pi u_0)|}$ &
$\frac{|R(\Pi u_0, \Pi\tilde{p})|}{|Q(u) - Q(\Pi u_0)|}$ & 
$\frac{|\tilde{R}(\tilde{\Pi} u_0, \tilde{p})|}{|Q(u) - Q(\tilde{\Pi} u_0)|}$ &\\
\hline
1.08 & 0.94 & 0.69 & 0.74 & 5 \\
\hline
& & 0.58 & 0.57 & 4\\
\hline
& & 0.53 & 0.37 & 3\\
\hline
& & 0.45 & 0.26 & 2\\
\hline
& & 0.36 & 0.15 & 1\\
\hline
\end{tabular}
\end{table}
%\end{block}

\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Example Calculations}

``Rough Problem"
%\begin{block}{``Smooth Problem"}
\begin{table}[width=\textwidth]
\begin{tabular}{|c|c|c|c|c|}
\hline
$\eta_{eff}$ & $\eta_{eff}$ & $\eta_{eff}$ & $\eta_{eff}$ & \footnotesize{\# of Enrich.} \\
\hline
$\frac{|R(\Pi u_0, p)|}{|Q(u) - Q(\Pi u_0)|}$ &  
$\frac{|R(\Pi u_0, \hat{p})|}{|Q(u) - Q(\Pi u_0)|}$ &
$\frac{|R(\Pi u_0, \Pi\tilde{p})|}{|Q(u) - Q(\Pi u_0)|}$ & 
$\frac{|\tilde{R}(\tilde{\Pi} u_0, \tilde{p})|}{|Q(u) - Q(\tilde{\Pi} u_0)|}$ &\\
\hline
1.47 & 0.72 & 0.64 & 0.67 & 5 \\
\hline
& & 0.59 & 0.57 & 4\\
\hline
& & 0.48 & 0.43 & 3\\
\hline
& & 0.39 & 0.32 & 2\\
\hline
& & 0.23 & 0.16 & 1\\
\hline
\end{tabular}
\end{table}
%\end{block}

\end{frame}


%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Example Calculations}

\begin{center}
\begin{columns}
\begin{column}{0.4\textwidth}
\centerline{``Smooth" Problem}
\centerline{\includegraphics[width=\textwidth]{smooth_adaptive}}
\end{column}
%
\begin{column}{0.4\textwidth}
\centerline{``Rough" Problem}
\centerline{\includegraphics[width=\textwidth]{rough_adaptive}}
\end{column}
\end{columns}
\end{center}

\end{frame}


%===============================================================================
% END OF FILE
%===============================================================================

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Mesh Adaptation}

\begin{block}{What is a mesh?}
\begin{itemize}
\item A collection of points (called nodes) connected together to form shapes
(called elements)
\item Over each element, we define a finite element that contains information
of the function representation (degrees of freedom, shape functions, etc)
\item The more elements and nodes we have, the better we can resolve the function
we are studying (solution of a differential equation)
\end{itemize}
\end{block}

\begin{block}{What do we mean by mesh adaptation/refinement?}
\begin{itemize}
\item We ``refine" a mesh by adding more degrees of freedom
\item This helps give better resolution to the function we wish to compute
\item Carefully choosing where to refine the mesh can \emph{greatly} accelerate
convergence to the desired solution
\end{itemize}
\end{block}

\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Types of Mesh Refinement}

\begin{block}{$h$-adaptivity}
\begin{itemize}
\item Break existing elements into smaller subelements
\item $h$, the size of the element, shrinks
\end{itemize}
\end{block}

\begin{block}{$p$-adaptivity}
\begin{itemize}
\item Raise the order of approximation of the basis functions
\item For example, make the element use quadratic basis functions instead of linear
\end{itemize}
\end{block}

\begin{block}{$hp$-adaptivity}
\begin{itemize}
\item Strategies combining both $h$-adaptivity and $p$-adaptivity
\item Use optimization algorithms to decide which (both?) types of adaptation to
perform on the element
\end{itemize}
\end{block}

\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Element $h$-refinement}

\begin{tabular}{ccc}\\
    \includegraphics[angle=-90, width=.45\textwidth]{triangle_refinement} &&
    \includegraphics[angle=-90, width=.45\textwidth]{quad_refinement} \\
    Triangle && Quadrilateral \\
    \includegraphics[angle=-90, width=.45\textwidth]{tet_refinement} &&
    \includegraphics[angle=-90, width=.45\textwidth]{prism_refinement}  \\
    Tetrahedron && Prism
  \end{tabular}

\vfill

\begin{footnotesize}
$^*$Images courtesy of Benjamin S. Kirk, Roy H. Stogner, and John W. Peterson
\end{footnotesize}

\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Hanging Nodes}

Locally $h$-refining elements naturally leads to the presence of \emph{hanging} nodes

\begin{block}{Dealing with Hanging Nodes}
\begin{itemize}
\item Can add buffer elements and/or force neighboring refinements to avoid hanging nodes
	\begin{itemize}
	\item Can cause unnecessary proliferation of element refinement
	\item Creating buffer zones, especially in 3D, can be challenging to implement
	\end{itemize}
\item More commonly, enforce continuity of the solution (for discontinuous methods, there's nothing to do)
	\begin{itemize}
	\item For Lagrange basis functions, simply interpolating is adequate
	\item For more exotic elements/formulations, need to be careful to preserve the approximation
	\item Resort to projection-based interpolation strategies: interpolate at the nodes, project onto the edge degrees of freedom, project onto faces, project into the interior
	\end{itemize}
\end{itemize}
\end{block}

\end{frame}



%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Nonconforming Meshes}

\begin{block}{}
\begin{itemize}
\item When hanging nodes are present, we say the mesh is \emph{nonconforming}
\item If there are, at most, $n$ hanging nodes on any element side, we say the mesh
is an $n$-irregular mesh
\item Although some software packages support more than 1-irregular meshes, it is
very uncommon for such meshes to be used
	\begin{itemize}
	\item Practical difficulty of implementation
	\item Enforcing 1-irregularity forces some transition between the size of the elements
	\item Can lose conformity of the approximation with greater than 1-irregularity~\cite{DemkowiczBook1}
	\end{itemize}
\end{itemize}
\end{block}

\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Mesh Redistribution}

\begin{block}{}
\begin{itemize}
\item Can adjust the mesh without changing its topology (how the elements/nodes are connected)
\item One common operation is mesh smoothing
	\begin{itemize}
	\item Generated meshes can have sharp interfaces, skewed elements, etc.
	\item Solve a PDE (typically a form of the Laplace equation) to adjust the position of the
	      nodes without changing the connectivity to improve the quality of the mesh
	\end{itemize}
\item A related idea is mesh redistribution where the nodes are redistributed to capture some
      desired feature
      \begin{itemize}
      \item One common example is shocks~\cite{BranetsDissertation}
      \end{itemize}
\end{itemize}
\end{block}

\end{frame}



%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
  \frametitle{Example: Compressible Flow Over a Wedge}

  \begin{itemize}[<+->]
    \item The problem studied is that of an oblique shock generated by a $10^o$ wedge angle.
      \begin{itemize}[<+->]
      \item This problem has an exact solution for density which is a step function.
%      \item Utilizing libmesh's exact solution capability the exact $L_2$ error can be solved for.
      \item The exact solution is shown below:
        \begin{figure}
          \begin{center}
            \includegraphics[viewport=20 10 660 600,clip=true,width=.4\textwidth]{shock}
          \end{center}
        \end{figure}
    \end{itemize}
  \end{itemize}

\begin{footnotesize}
$^*$Slide courtesy of Benjamin S. Kirk, Roy H. Stogner, and John W. Peterson
\end{footnotesize}

\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
  \frametitle{Uniformly Refined Solutions}

  \begin{itemize}[<+->]
  \item For comparison purposes, here is a mesh and a solution after 1 uniform refinement with 10890 DOFs.
    \begin{figure}[!htb]
      \begin{center}
        \subfigure[Mesh after 1 uniform refinement.]{\label{fig:fob_uniform_2_mesh}\includegraphics[viewport=110 30 600 550,clip=true,width=.42\textwidth]{fob_uniform_2_mesh}}
        \subfigure[Solution after 1 uniform refinement.]{\label{fig:fob_uniform_2_sol}\includegraphics[viewport=110 30 600 520,clip=true,width=.42\textwidth]{fob_uniform_2_sol}}
      \end{center}
    \end{figure}
  \end{itemize}

\begin{footnotesize}
$^*$Slide courtesy of Benjamin S. Kirk, Roy H. Stogner, and John W. Peterson
\end{footnotesize}

\end{frame}



%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}

  \frametitle{$h$-Adapted Solutions}
  \begin{itemize}[<+->]
    \item A flux jump indicator was employed as the error indicator %along with a statistical flagging scheme.
    \item Here is a mesh and solution after 2 adaptive refinements containing 10800 DOFs:
      \begin{figure}[!htb]
        \begin{center}
          \subfigure[Mesh, 2 refinements]{\label{fig:fob_adapt_3_mesh}\includegraphics[viewport=110 30 600 550,clip=true,width=.4\textwidth]{fob_adapt_3_mesh}}
          \subfigure[Solution]{\label{fig:fob_adapt_3_sol}\includegraphics[viewport=110 30 600 520,clip=true,width=.4\textwidth]{fob_adapt_3_sol}}
        \end{center}
      \end{figure}
  \end{itemize}

\begin{footnotesize}
$^*$Slide courtesy of Benjamin S. Kirk, Roy H. Stogner, and John W. Peterson
\end{footnotesize}

\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Redistributed Solutions}

  \begin{itemize}[<+->]
    \item Redistribution utilizing the same flux jump indicator.
      \begin{figure}[!htb]
        \begin{center}
          \subfigure[Mesh, 8 redistribution steps]{\label{fig:fob_redist_adapt_8_mesh}\includegraphics[viewport=110 30 600 550,clip=true,width=.42\textwidth]{fob_redist_adapt_8_mesh}}
          \subfigure[Solution]{\label{fig:fob_redist_adapt_8_sol}\includegraphics[viewport=110 30 600 520,clip=true,width=.42\textwidth]{fob_redist_adapt_8_sol}}
        \end{center}
      \end{figure}
  \end{itemize}

\begin{footnotesize}
$^*$Slide courtesy of Benjamin S. Kirk, Roy H. Stogner, and John W. Peterson
\end{footnotesize}

\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Redistributed and Adapted}

  \begin{itemize}[<+->]
    \item Now combining the two, here are the mesh and solution after 2 adaptations beyond the previous redistribution containing 10190 DOFs.
      \begin{figure}[!htb]
        \begin{center}
          \subfigure[Mesh, 2 refinements]{\label{fig:fob_redist_adapt_10_mesh}\includegraphics[viewport=110 30 600 550,clip=true,width=.4\textwidth]{fob_redist_adapt_10_mesh}}
          \subfigure[Solution]{\label{fig:fob_redist_adapt_10_sol}\includegraphics[viewport=110 30 600 520,clip=true,width=.4\textwidth]{fob_redist_adapt_10_sol}}
        \end{center}
      \end{figure}
  \end{itemize}

\begin{footnotesize}
$^*$Slide courtesy of Benjamin S. Kirk, Roy H. Stogner, and John W. Peterson
\end{footnotesize}

\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Solution Comparison}

  \begin{itemize}[<+->]
    \item For a better comparison here are 3 of the solutions, each with around 11000 DOFs:
      \begin{figure}[!htb]
        \begin{center}
          \subfigure[Uniform.]{\label{fig:fob_uniform_2_sol}\includegraphics[viewport=110 30 600 520,clip=true,width=.3\textwidth]{fob_uniform_2_sol}}
          \subfigure[Adaptive.]{\label{fig:fob_adapt_3_sol}\includegraphics[viewport=110 30 600 520,clip=true,width=.3\textwidth]{fob_adapt_3_sol}}
          \subfigure[R + H.]{\label{fig:fob_redist_adapt_10_sol}\includegraphics[viewport=110 30 600 520,clip=true,width=.3\textwidth]{fob_redist_adapt_10_sol}}
        \end{center}
      \end{figure}
  \end{itemize}

\begin{footnotesize}
$^*$Slide courtesy of Benjamin S. Kirk, Roy H. Stogner, and John W. Peterson
\end{footnotesize}

\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Error Plot}

  \begin{itemize}[<+->]
    %\item libmesh provides capability for computing error norms against an exact solution.
    \item The exact solution is not in $H^1$ therefore we only look at the $L_2$ convergence plot:
      \begin{figure}[!htb]
      \begin{center}
        \subfigure[LogLog plot of L2 vs DOFs.]{\label{fig:fob_l2}\includegraphics[viewport=0 10 600 400,clip=true,width=.7\textwidth]{fob_l2}}
      \end{center}
      \end{figure}
  \end{itemize}

\begin{footnotesize}
$^*$Slide courtesy of Benjamin S. Kirk, Roy H. Stogner, and John W. Peterson
\end{footnotesize}

\end{frame}

%===============================================================================
% NEW SLIDE
%===============================================================================
\begin{frame}
\frametitle{Mesh Adaptation}

\begin{block}{How to Choose Where to Refine the Mesh?}
\begin{itemize}
\item This is the job of local \emph{error indicators}
\item We construct an estimate of the error $\eta$
\item That error estimate is partitioned over the elements $\eta = \sum_K \eta_K$
\item Then we can use various flagging strategies to label which elements to refine
	\begin{itemize}
	\item Error value
	\item Error fraction
	\item Target number of elements (optimization)
	\end{itemize}
\end{itemize}
\end{block}

\pause

\begin{center}
The job of error estimation is to come up with $\eta$ and $\eta_K$
\end{center}

\end{frame}

%===============================================================================
% END OF FILE
%===============================================================================
